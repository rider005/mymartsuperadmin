import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:my_mart_super_admin/otherFiles/Constants.dart';
import 'package:my_mart_super_admin/services/AuthServices.dart';
import 'package:my_mart_super_admin/utility/Utilities.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();

  var userEmail = '';
  var userPass = '';
  var utility = Utilities();
  var authHandler = AuthServices();

  _onLoginBtnClickHandler() async {
    if (_formKey.currentState!.validate()) {
      try {
        var user = await authHandler.handleSignInEmail(
            userEmail.toLowerCase(), userPass);
        if ((await user.getIdTokenResult(true))
            .claims!
            .containsKey('isSuperAdmin')) {
          ScaffoldMessenger.of(context)
              .showSnackBar(utility.snackBar('Login Success as Super Admin'));
          Navigator.of(context).pushNamedAndRemoveUntil(
              DASHBOARD_SCREEN, (Route<dynamic> route) => false);
        } else {
          try {
            await authHandler.logOutUser();
          } catch (e) {
            print(e);
          }
          ScaffoldMessenger.of(context).showSnackBar(
              utility.snackBar("You don't have permission for this app"));
        }
      } on FirebaseAuthException catch (e) {
        if (e.message!.contains('There is no user')) {
          ScaffoldMessenger.of(context)
              .showSnackBar(utility.snackBar('There is no user'));
        } else if (e.message!.contains('password is invalid')) {
          ScaffoldMessenger.of(context).showSnackBar(
              utility.snackBar('password is invalid, try forget-password...'));
        } else {
          ScaffoldMessenger.of(context)
              .showSnackBar(utility.snackBar(e.message!));
        }
      }
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(utility.snackBar('Enter valid email & password'));
    }
  }

  _onForgetPass() {
    authHandler
        .forgetPass(userEmail)
        .then((value) => {
              ScaffoldMessenger.of(context).showSnackBar(
                  utility.snackBar('Forget password email send success'))
            })
        .catchError((onError) => {
              ScaffoldMessenger.of(context).showSnackBar(
                  utility.snackBar('Write valid email or Try again'))
            });
  }

  @override
  Widget build(BuildContext context) {
    var colWidth = 0.40;
    if (kIsWeb) {
      //running on web
      colWidth = 0.40;
    } else if (Platform.isAndroid || Platform.isIOS) {
      //running on android or ios device
      colWidth = 0.60;
    }
    return Scaffold(
        // resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: const Text('Login'),
        ),
        body: Form(
          key: _formKey,
          child: Center(
            child: FractionallySizedBox(
                widthFactor: colWidth, // between 0 and 1
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Enter your email',
                          hintText: 'Enter email here',
                        ),
                        textAlign: TextAlign.center,
                        initialValue: userEmail,
                        onChanged: (userEmailChange) =>
                            setState(() => userEmail = userEmailChange),
                        validator: (email) => utility.validateEmail(email!),
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                    TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Enter your password',
                          hintText: 'Enter password here',
                          suffixIcon: Icon(Icons.lock),
                        ),
                        textAlign: TextAlign.center,
                        initialValue: userPass,
                        onChanged: (userPassChange) =>
                            setState(() => userPass = userPassChange),
                        validator: (pass) => utility.validatePass(pass!),
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                    Align(
                        alignment: Alignment.topRight,
                        child: GestureDetector(
                            onTap: () {
                              _onForgetPass();
                            },
                            child: const Text('Forget Password'))),
                    OutlinedButton(
                      onPressed: _onLoginBtnClickHandler,
                      child: const Text('Login'),
                    ),
                  ]
                      .map((e) => Padding(
                            child: e,
                            padding: const EdgeInsets.symmetric(vertical: 10),
                          ))
                      .toList(),
                )),
          ),
        ));
  }
}
