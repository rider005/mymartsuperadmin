import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:my_mart_super_admin/services/AuthServices.dart';
import 'package:my_mart_super_admin/services/FirestoreServices.dart';
import 'package:my_mart_super_admin/utility/Utilities.dart';

class AddNewUserScreen extends StatefulWidget {
  const AddNewUserScreen({Key? key}) : super(key: key);

  @override
  State<AddNewUserScreen> createState() => _AddNewUserScreenState();
}

class _AddNewUserScreenState extends State<AddNewUserScreen> {
  final _formKey = GlobalKey<FormState>();
  var userName = '';
  var userEmail = '';
  var userPass = '';
  var utility = Utilities();

  _onRegisterBtnClickHandler() {
    var authHandler = AuthServices();
    var firestoreHandler = FirestoreServices();
    authHandler
        .handleSignUp(userEmail, userPass)
        .then((userData) => {
              print(userData),
              firestoreHandler
                  .addUser(userEmail, userData.uid)
                  .then((value1) => {})
                  .catchError((onError) => {})
            })
        .catchError((onError) => print(onError));
  }

  @override
  Widget build(BuildContext context) {
    var colWidth = 0.40;
    if (kIsWeb) {
      //running on web
      colWidth = 0.40;
    } else if (Platform.isAndroid || Platform.isIOS) {
      //running on android or ios device
      colWidth = 0.60;
    }
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Form(
            key: _formKey,
            child: FractionallySizedBox(
                widthFactor: colWidth, // between 0 and 1
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Enter your email',
                          hintText: 'Enter email here',
                        ),
                        textAlign: TextAlign.center,
                        initialValue: userEmail,
                        onChanged: (userEmailChange) =>
                            setState(() => userEmail = userEmailChange),
                        validator: (email) => utility.validateEmail(email!),
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                    TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Enter your email',
                          hintText: 'Enter email here',
                        ),
                        textAlign: TextAlign.center,
                        initialValue: userEmail,
                        onChanged: (userEmailChange) =>
                            setState(() => userEmail = userEmailChange),
                        validator: (email) => utility.validateEmail(email!),
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                    TextFormField(
                        decoration: const InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Enter your password',
                          hintText: 'Enter password here',
                          suffixIcon: Icon(Icons.lock),
                        ),
                        textAlign: TextAlign.center,
                        initialValue: userPass,
                        onChanged: (userPassChange) =>
                            setState(() => userPass = userPassChange),
                        validator: (pass) => utility.validatePass(pass!),
                        autovalidateMode: AutovalidateMode.onUserInteraction),
                    // Align(
                    //     alignment: Alignment.topRight,
                    //     child: Text('Forget Password')),
                    OutlinedButton(
                      onPressed: _onRegisterBtnClickHandler,
                      child: const Text('Register'),
                    ),
                  ]
                      .map((e) => Padding(
                            child: e,
                            padding: const EdgeInsets.symmetric(vertical: 10),
                          ))
                      .toList(),
                )),
          )
          // OutlinedButton(
          //   onPressed: () => {print('logout button pressed')},
          //   child: Text('Logout'),
          // ),
        ],
      ),
    );
  }
}
