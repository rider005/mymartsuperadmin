import 'package:flutter/material.dart';
import 'package:my_mart_super_admin/services/FirestoreServices.dart';
import 'package:my_mart_super_admin/utility/Utilities.dart';

class AllUsersListScreen extends StatefulWidget {
  const AllUsersListScreen({Key? key}) : super(key: key);

  @override
  _AllUsersListState createState() => _AllUsersListState();
}

class _AllUsersListState extends State<AllUsersListScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(kToolbarHeight),
          child: AppBar(
            bottom: const TabBar(
              tabs: [
                Tab(
                  text: 'All Super Admins',
                ),
                Tab(
                  text: 'All Admins',
                ),
              ],
            ),
          ),
        ),
        body: TabBarView(
          children: [
            Utilities()
                .getListWidget(FirestoreServices().getSuperAdminUsersCollRef()),
            Utilities()
                .getListWidget(FirestoreServices().getAdminUsersCollRef()),
          ],
        ),
      ),
    );
  }
}
