import 'package:cloud_firestore/cloud_firestore.dart';

class FirestoreServices {
  CollectionReference adminUsers =
      FirebaseFirestore.instance.collection('allAdmins');

  Future<void> addUser(String email, String uId) {
    // Call the user's CollectionReference to add a new user
    return adminUsers.doc(uId).set({
      'userEmail': email,
      'userName': 'userName',
    });
  }

  CollectionReference<Map<String, dynamic>> getSuperAdminUsersCollRef() {
    return FirebaseFirestore.instance.collection('allSuperAdmins');
  }

  CollectionReference<Map<String, dynamic>> getAdminUsersCollRef() {
    return FirebaseFirestore.instance.collection('allAdmins');
  }
}
