import 'dart:convert';
import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_mart_super_admin/services/FirestoreServices.dart';

class Utilities {
  validateUserName(String userName) {
    if (userName.isEmpty) {
      return 'User Name is required';
    } else if (!RegExp(r'^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$')
        .hasMatch(userName)) {
      return 'Please enter valid user name';
    } else {
      return null;
    }
  }

  validateEmail(String userEmail) {
    if (userEmail.isEmpty) {
      return 'Email is required';
    } else if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(userEmail)) {
      return 'Please enter valid email';
    } else {
      return null;
    }
  }

  validatePass(String userPass) {
    if (userPass.isEmpty) {
      return 'Password is required';
    } else if (!RegExp(
            r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
        .hasMatch(userPass)) {
      return 'Please enter valid password';
    } else {
      return null;
    }
  }

  snackBar(String message) {
    return SnackBar(content: Text(message));
  }

  getListWidget(collRef) {
    var usersList = [];
    return FutureBuilder(
        future: collRef.get().then(
            (QuerySnapshot querySnapshot) => {usersList = querySnapshot.docs}),
        builder: (ctx, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              !snapshot.hasError) {
            return ListView.builder(
              itemBuilder: (context, position) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Text(usersList[position]['userName']),
                          Text(usersList[position]['userEmail']),
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: usersList.length,
            );
          } else {
            return const CircularProgressIndicator();
          }
        });
  }
}
