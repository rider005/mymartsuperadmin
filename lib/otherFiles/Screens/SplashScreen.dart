import 'dart:async';
import 'package:flutter/material.dart';
import 'package:my_mart_super_admin/BottomNavigationScreen.dart';
import 'package:my_mart_super_admin/auth/Login.dart';
import 'package:my_mart_super_admin/services/AuthServices.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    var isLoggedIn = false;
    var authHandler = AuthServices();
    authHandler
        .getCurrentUser()
        .then((user) => {isLoggedIn = true, showNextScreen(isLoggedIn)})
        .catchError((onError) => {print(onError), showNextScreen(isLoggedIn)});
  }

  showNextScreen(bool isLoggedIn) {
    Timer(
        const Duration(seconds: 1),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => !isLoggedIn
                ? const LoginScreen()
                : const BottomNavigationScreen())));
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Text('Splash Screen'),
      ),
    );
  }
}
