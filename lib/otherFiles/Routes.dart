import 'package:flutter/cupertino.dart';
import 'package:my_mart_super_admin/auth/Login.dart';

import '../BottomNavigationScreen.dart';
import 'Constants.dart';

var routesList = <String, WidgetBuilder>{
  LOGIN_SCREEN: (BuildContext context) => const LoginScreen(),
  DASHBOARD_SCREEN: (BuildContext context) => const BottomNavigationScreen(),
};
