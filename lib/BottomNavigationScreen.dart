import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:my_mart_super_admin/AllUsersList.dart';
import 'package:my_mart_super_admin/auth/AddNewUser.dart';

class BottomNavigationScreen extends StatefulWidget {
  const BottomNavigationScreen({Key? key}) : super(key: key);

  @override
  State<BottomNavigationScreen> createState() => _MyBottomNavStatefulWidget();
}

class _MyBottomNavStatefulWidget extends State<BottomNavigationScreen> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const TAB_NAME = [
    'Add Admin User',
    'All Users List',
    'School',
    'Profile'
  ];

  // var authHandler = new AuthServices();
  var divideHeightBy = 0;
  var _navTitle = TAB_NAME[0];

  static final List<Widget> _widgetOptions = <Widget>[
    const AddNewUserScreen(),
    const AllUsersListScreen(),
    // const Text(
    //   'Index 1: Business',
    //   style: optionStyle,
    // ),
    const Text(
      'Index 2: School',
      style: optionStyle,
    ),
    const Text(
      'Index 3: Profile',
      style: optionStyle,
    )
  ];

  void _onItemTapped(int index) {
    setState(() {
      switch (index) {
        case 0:
          _navTitle = TAB_NAME[0];
          break;
        case 1:
          _navTitle = TAB_NAME[1];
          break;
        case 2:
          _navTitle = TAB_NAME[2];
          break;
        case 3:
          _navTitle = TAB_NAME[3];
          break;
      }
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    // final screenWidth = MediaQuery.of(context).size.width;
    // if (screenWidth < 600.0) {
    //   // return _buildPhoneLayout();
    // } else if (screenWidth < 1000.0) {
    //   // return _buildTabletLayout();
    // } else {
    //   // return _buildDesktopLayout();
    // }
    if (kIsWeb) {
      //running on web
      divideHeightBy = 2;
    } else if (Platform.isAndroid || Platform.isIOS) {
      //running on android or ios device
      divideHeightBy = 4;
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(_navTitle),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: const Icon(Icons.home),
            label: TAB_NAME[0],
            backgroundColor: Colors.red,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.business),
            label: TAB_NAME[1],
            backgroundColor: Colors.green,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.school),
            label: TAB_NAME[2],
            backgroundColor: Colors.purple,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.account_circle),
            label: TAB_NAME[3],
            backgroundColor: Colors.pink,
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
